Shyqueue
====

v2.2がリリースされました. [ShyQueue](https://shy-queue.herokuapp.com/users)で利用できます.

## Demo

#### スマホ版ShyQueue
![スマホ版ShyQueue](./public/ShyQueue_mobile.jpeg)

## 機能

ログイン後, リストからユーザーを選択して, メールが送信できます. 初回ログインはユーザー登録を兼ねています.


## benri command

herokuコマンドが通るようにする
```
heroku login
```

herokuへのデプロイ
```
git push heroku release:master
```

dbのデータをリセット
```
heroku run rake db:reset
```

## 起動方法

Heroku上で動作します. すでに起動しているので, [ShyQueue](https://shy-queue.herokuapp.com/users)にアクセスするだけで利用できます.

#### Herokuのアカウント作成

[Heroku](https://dashboard.heroku.com)で作成.

#### GemをHeroku用に変更

デフォルトでsqlite3を使わないようにする

#### ソースコードをHeroku上にデプロイ

Heroku CLIツールとGitを利用.
`git push heroku release:master`や`git push heroku master`でデプロイする.

#### DBの設定
`heroku run`コマンドでheroku上のbashを操作できる.

```
heroku run rake db:migrate
heroku run rake db:seed_fu
```

#### アクセス
`heroku open`でブラウザを開き, `heroku logs --tail`でlogを観られる

## 注意

ローカルから起動するときと, Herokuで起動するときに利用するSMTPサーバーが異なる. ローカル時はGmail, Heroku時はSendGridを利用している.

ローカル時の設定は`config/environments/development.rb`にあり, Heroku時の設定は`config/environment.rb`にある.

## v2.1からの変更点

+ デザインを変更しました
+ helpページが追加されました
+ 画面遷移のルートが追加されました (navbar -> index)

## 課題
* SPAじゃない
* adminLTEを適用したい
