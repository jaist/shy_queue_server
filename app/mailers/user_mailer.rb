class UserMailer < ActionMailer::Base
  default from: "yoo46ra@gmail.com"

  def post_email(user)
    logger.debug("[UserMailer]<post_email> fire")
    @name = user.name
    @count = user.count
    logger.debug("[UserMailer]<post_email> user.email: " + user.email.to_s)
    mail to: user.email, subject: "あなたとお話したい人が現れました"
  end
end
