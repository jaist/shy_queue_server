class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_filter :require_login, only: [:new, :create]

  def index
    @users = User.all
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'Dancer was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def send_mail
    logger.debug("[USER CTR]<send_mail> fire")
    users = params[:users]
    logger.debug("[USER CTR]<send_mail> users: #{users} ")
    logger.debug("[USER CTR]<send_mail> メールを送ります. ")
    users.each do |u|
      if u.length == 2 then
        @user = User.find(u[:id])
        logger.debug("[USER CTR]<send_mail> @user.count: #{@user.count} ")
        logger.debug("[USER CTR]<send_mail> @user.id: #{@user.id} ")
        count = User.find(@user.id).count
        logger.debug("[USER CTR]<send_mail> count: #{count} ")
        if count == nil then
          logger.debug("[USER CTR]<send_mail> カウント初期化")
          count = 0
        end
        count = count + 1
        @user.update(count: count)
        logger.debug("[USER CTR]<send_mail> @user.count: #{@user.count} ")
        logger.debug("[USER CTR]<send_mail> @user_id: #{@user.id}")
        UserMailer.post_email(@user).deliver_now
      end
    end
    respond_to do |format|
        format.html { redirect_to users_path, notice: 'メールが送信されました' }
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :sex, :age, :image, :introduction, :count, :password, :password_confirmation)
    end
end
