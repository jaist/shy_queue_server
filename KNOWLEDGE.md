KNOWLEDGE
====

## どうやってRailsからメールを送るのか

Action Mailer機能を利用する.

#### メーラーを作成

```
rails g mailer UserMailer
```

#### メール送信メソッドを実装

生成されたメーラー関数内に送信メソッドを書いて行く

```{user_mailer.rb}
class UserMailer < ActionMailer::Base
  default from: "s1550020@jaist.ac.jp"

  def post_email(user)
    logger.debug("[PostMailer post_email] : fire")
    @name = user.name
    @count = user.count
    logger.debug("[PostMailer post_email] user.email: " + user.email.to_s)
    mail to: user.email, subject: "あなたとお話したい人が現れました"
  end
end
```

#### メーラービューの作成

メールのレイアウトをviewsの中にhtmlとして記述

```{views/user_mailer/user_email.html.erb}
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
  </head>
  <body>
    <h1>あなたとお話をしたい人が現れました</h1>
    <p>あなたはこれまでに<%= @count %>回、連絡をいただいています。</p>
  </body>
</html>

```

#### メーラーの呼び出し

```
def send_mail
  logger.debug("[send_mail]")
  users = params[:users]
  logger.debug(users)
  # users : [{"id"=>"1", "user"=>"1"}, {"id"=>"2", "user"=>"1"}, {"id"=>"3"}]
  logger.debug("[send_mail] メールおくりまーす")
  users.each do |user|
    if user.length == 2 then
      logger.debug(user)
      user = User.find(user[:id])
      count = User.find(user[:id])[:count]
      count = count + 1
      user.update( count: count)
      logger.debug("[send_mail] user_id: " + user[:id].to_s)
      PostMailer.post_email(user).deliver
    end
  end
  respond_to do |format|
      format.html { redirect_to users_path, notice: 'メールが送信されました' }
  end
end
```

#### 疑問

`/config/environments/development.rb`でmailerのIDやpasswordを環境変数(ENV)を使うと, うまく行かない. 直にpasswordを書くとうまくメールを送れる. why???

#### 参考文献
[RailsのAction Mailerでメール送信](http://ruby-rails.hatenadiary.com/entry/20140828/1409236436)

[RailsのActionMailerでGmailを使ったらNet::SMTPAuthenticationErrorが出た](http://qiita.com/aokabin/items/704fe30c33b885ac14f1)

## bootstrapをrailsに導入
[RailsにTwitter Bootstrapの導入と簡易な使い方](http://ruby-rails.hatenadiary.com/entry/20140801/1406818800)

#### Gemをインストール
```
gem 'therubyracer' # javascript runtime。lessをコンパイルするために必要
gem 'less-rails' # Railsでlessを使えるようにする。Bootstrapがlessで書かれているため
gem 'twitter-bootstrap-rails' # Bootstrapの本体
```

#### ジェネレータでbootstrap fileを生成
```
rails g bootstrap:install
```

## herokuからメールを使うために

##### 追加アドオンが必要
[【無料】herokuからメールを送信する](http://shoma2da.hatenablog.com/entry/2014/03/02/113908)
sendgridを利用
```
heroku addons:add sendgrid
```

[SendGrid](https://devcenter.heroku.com/articles/sendgrid#ruby)
sendgridのgemを利用
```
gem 'sendgrid-ruby'
bundle install
```

#### SendGridの設定
[Railsのメール配信サーバーについてメモ書き](http://qiita.com/masarufuruya/items/3150f72104e3ca453c68)
[HerokuアドオンのSendGridをRailsアプリから使う](https://cloudautomator.com/blog/2014/09/24/sendgrid-with-heroku-and-rails/)

SendGridのアドレスとパスワードを調べる. addon追加時に自動的に環境変数にセットされているので、環境変数の値を調べると良い.
```
heroku config --app shy-queue
```

SMTPの設定を変更する. Herokuで動かすのでproduction環境をいじる. もしくはactionmailerのbaseをいじる
