class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :sex
      t.integer :age
      t.string :image
      t.string :introduction
      t.integer :count

      t.timestamps null: false
    end
  end
end
