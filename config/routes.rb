Rails.application.routes.draw do
  root :to => 'users#index'
  get 'user_sessions/new'
  get 'user_sessions/create'
  get 'user_sessions/destroy'
  resources :records
  resources :users
  resources :user_sessions
  get 'help' => 'help#help', :as => :help
  post 'send_mail' => 'users#send_mail'
  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout
end
